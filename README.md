# Qualitätssicherung
Testbericht des PSE-Projekts "Webservice zur Definition und Durchsetzung des Mehr-Augen-Prinzips" im WS2018/19 von Julius Häcker, Moritz Leitner, Nicolas Schuler, Noah Wahl und Wendy Yi

### PDF
Testbericht:
```console
cd Document/
pdflatex Testbericht.tex
pdflatex Testbericht.tex
```